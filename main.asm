%define NEW_LINE	0xA
%define BUFFER_SIZE	256
%include "words.inc"
%include "lib.inc"
extern find_word
section .rodata
key_input:	db "Enter a key to find value", NEW_LINE, 0
key_too_long:	db "A key is too long for buffer, try to enter another one", NEW_LINE, 0
key_found:	db "A key has been found", NEW_LINE, 0
key_absent:	db "There is no such key", NEW_LINE, 0
section .bss
buffer: resb BUFFER_SIZE
section .data
global _start
section .text
_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	push rsi
	push rdi
	call read_word
	pop rdi
	pop rsi
	cmp rax, 0
	je .too_long_key
	mov rdi, buffer
	mov rsi, FIRST_ELEMENT
	call find_word
	cmp rax, 0
	je .key_absent
	push rax
	push rdi
	mov rdi, key_found
	call print_string
	pop rdi
	pop rax
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	mov rdi, rax
	call print_string
	mov rdi, 0xA
	call print_char
	call exit
	.key_absent:
		mov rdi, key_absent
		call print_string
		call exit
	.too_long_key:
		mov rdi, key_too_long
		call print_string
		call exit
