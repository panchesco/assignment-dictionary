%include "lib.inc"
global find_word
find_word:		
	.loop:
		test rsi, rsi		;if next entry point, then we reached the end of linkedList	
		je .nf			
		push rsi		
		push rdi
		add rsi, 8		;add 8 to rsi due to reach a key
		call string_equals	
		pop rdi
		pop rsi
		cmp rax, 1		;if string_equals returned 1, then we found a key in list
		je .f
		mov rsi, [rsi]		
		jmp .loop

	.nf:
		xor rax, rax
		ret
	.f:
		mov rax, rsi       ; entry point of map to rax and ret
		ret