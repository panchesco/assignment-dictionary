asm = nasm -felf64
ld = ld -o
all: lib.asm dict.asm main.asm colon.inc words.inc
	$(asm) dict.asm
	$(asm) lib.asm
	$(asm) main.asm
	$(ld) main dict.o main.o lib.o 
lib.o: lib.asm
	$(asm) $<

dict.o: dict.asm
	$(asm) $<

main.o:main.asm colon.inc words.inc
	$(asm) $<

main: main.o dict.o lib.o
	$(ld) $@ $^

